export default class HeadBase extends HTMLElement {
	static get observedAttributes() {
		return ["href", "target"];
	}
	get head() {
		return window.document.querySelector("head");
	}
	get href() {
		return this.getAttribute("href") || this.defaultHref;
	}
	set href(str) {
		this.setAttribute("href", str || this.defaultHref);
	}
	get defaultHref() {
		/* @TODO: (unusable) relative to where this file is located */
		return new URL("../../../", import.meta.url).pathname;
	}
	constructor() {
		super();
	}
	attributeChangedCallback(name, newVal, oldVal) {
		if (oldVal !== newVal) {
			this.init();
		}
	}
	connectedCallback() {
		this.init();
	}
	async init() {
		this.$base = this._createBase();
		this.render();
	}
	render() {
		const $existingBase = this.head.querySelector(`base`);
		if ($existingBase) {
			$existingBase.remove();
		}
		this.head.append(this.$base);
	}
	_createBase() {
		const $base = document.createElement("base");
		if (this.href) {
			$base.setAttribute("href", this.href);
		}
		if (this.target) {
			$base.setAttribute("target", this.target);
		}
		return $base;
	}
}
