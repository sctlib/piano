import { createElement, SITE_BASE } from "../js-utils/index.js";

export default class BodyMenu extends HTMLElement {
	static get observedAttributes() {
		return ["items", "href"];
	}
	get items() {
		let jsonItems;
		try {
			jsonItems = JSON.parse(this.getAttribute("items"));
		} catch (e) {
			console.warn(e);
		}
		return jsonItems || [];
	}
	get href() {
		const baseUrl = SITE_BASE;
		return this.getAttribute("href") || baseUrl;
	}
	constructor() {
		super();
	}
	connectedCallback() {
		if (this.items.length) {
			this.$doms = this._createDoms(this.items);
			this._render();
		}
	}
	_render() {
		this.innerHTML = "";
		this.append(...this.$doms);
	}
	_createDoms(menuItems) {
		const $items = menuItems
			.map((item) => {
				if (typeof item === "object") {
					return createElement({
						...item,
						element: "a",
						href: `${this.href}/${item.href || item}/`,
					});
				} else {
					return createElement({
						element: "a",
						innerText: item,
						href: `${this.href}/${item}/`,
					});
				}
			})
			.map(($menuItem) => {
				/* wrap them in a list */
				const $li = createElement("li");
				$li.append($menuItem);
				return $li;
			});
		const $menu = createElement("menu");
		$menu.append(...$items);
		return [$menu];
	}
}
