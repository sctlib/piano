import { SITE_BASE } from "../js-utils/index.js";

export default class HeadFavicon extends HTMLElement {
	static get observedAttributes() {
		return ["href"];
	}
	get head() {
		return window.document.querySelector("head");
	}
	get rel() {
		return "shortcut icon";
	}
	get href() {
		return this.getAttribute("href") || this.defaultHref;
	}
	set href(str) {
		this.setAttribute("href", str || this.defaultHref);
	}
	get defaultHref() {
		return `/favicon.ico`;
	}
	get faviconHrefs() {
		return ["/assets/favicon", "/favicon", "/logo", "/assets/logo"];
	}
	get faviconExtensions() {
		return ["svg", "ico", "png", "jpg"];
	}
	constructor() {
		super();
	}
	attributeChangedCallback(name, newVal, oldVal) {
		if (oldVal !== newVal) {
			this.init();
		}
	}
	connectedCallback() {
		this.init();
	}
	async init() {
		this.$linkFavicon = this._createLink();
		let matchedHref;
		try {
			matchedHref = await this._tryFetchFaviconUrl();
		} catch (e) {
			// silent error, no favicon found
		}
		if (matchedHref) {
			this.href = matchedHref;
		}
		this.render();
	}
	async _tryFetchFaviconUrl() {
		let matchedHref;
		for (const nextHref of this.faviconHrefs) {
			for (const ext of this.faviconExtensions) {
				const hrefWithExt = `${nextHref}.${ext}`;
				try {
					const href = this._buildSiteHref(hrefWithExt);
					const response = await fetch(href);
					if (response.ok) {
						matchedHref = href;
						break; // Stop fetching if a favicon is found
					}
				} catch (error) {
					// Handle fetch error
				}
			}
			if (matchedHref) {
				break; // Stop iterating over faviconHrefs if a favicon is found
			}
		}
		return matchedHref;
	}
	/* so the url is relative to the path of this file, also in /assets,
	 and the favicon works if deployment on `/piano/` and `/` */
	_buildSiteHref(href) {
		return SITE_BASE + href;
	}
	render() {
		const $existingFavicon = this.head.querySelector(
			`link[rel="${this.rel}"][href]`
		);
		if ($existingFavicon) {
			$existingFavicon.remove();
		}
		this.head.append(this.$linkFavicon);
	}
	_createLink() {
		const $link = document.createElement("link");
		$link.setAttribute("rel", this.rel);
		$link.setAttribute("href", this.href);
		return $link;
	}
}
