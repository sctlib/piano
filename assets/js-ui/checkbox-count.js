export default class CheckboxCount extends HTMLElement {
	get showGroups() {
		return this.getAttribute("show-groups") === "true";
	}

	constructor() {
		super();
		this._checkboxListeners = [];
	}
	connectedCallback() {
		this.$checkboxes = document.querySelectorAll(`input[type="checkbox"]`);
		this.$checkboxes.forEach(($checkbox) => {
			$checkbox.addEventListener("input", this.onInput.bind(this));
		});
		const $checked = Array.from(this.$checkboxes).filter(
			($checkbox) => $checkbox.checked
		);
		this.total = this.$checkboxes.length;
		this.checked = $checked.length;
		this.render();
	}
	disconnectedCallback() {
		if (this.$checkboxes) {
			this.$checkboxes.forEach(($checkbox) => {
				$checkbox.removeEventListener("input", this.onInput.bind(this));
			});
		}
	}
	onInput(event) {
		console.log(event.target.checked);
		if (event.target.checked) {
			this.checked++;
		} else {
			this.checked--;
		}
		this.render();
	}
	render() {
		this.innerText = `${this.checked}/${this.total}`;
	}
}
