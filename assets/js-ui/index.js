import { defineComponents, insertComponents } from "../js-utils/index.js";

import HeadBase from "./head-base.js";
import HeadFavicon from "./head-favicon.js";
import BodyMenu from "./body-menu.js";
import CheckboxCount from "./checkbox-count.js";

const componentDefinitions = {
	"head-base": HeadBase,
	"head-favicon": HeadFavicon,
	"body-menu": BodyMenu,
	"checkbox-count": CheckboxCount,
};

/* auto define all components when importing from here */
defineComponents(componentDefinitions);

/*
	 - a function to init the ui;
	 - this file and imported should not be part of the
	 piano js library
	 - it inserts all elements that are not part of the "content",
	 for example those that should come on all pages (piano-app)
 */
const menuItems = [
	{
		innerText: "home",
		href: ".",
	},
	"info",
	"tools",
	{
		innerText: "exercises",
		href: "ex",
	},
];
const init = () => {
	insertComponents({
		headElements: [
			/* "head-base", */
			"head-favicon",
		],
		bodyStartElements: [
			{
				element: "body-menu",
				items: menuItems,
			},
		],
		bodyEndElements: ["piano-app"],
	});
};

init();

export { componentDefinitions };
export default init;
