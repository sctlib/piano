/* All notes of the chormatic scale, starting from C */
const CHROMATIC_SCALE_NOTES = [
	{
		name: "C",
		latin: "do",
		color: "white",
	},
	{
		name: "C#",
		latin: "do#",
		color: "black",
	},
	{
		name: "D",
		latin: "re",
		color: "white",
	},
	{
		name: "D#",
		latin: "re#",
		color: "black",
	},
	{
		name: "E",
		latin: "mi",
		color: "white",
	},
	{
		name: "F",
		latin: "fa",
		color: "white",
	},
	{
		name: "F#",
		latin: "fa#",
		color: "black",
	},
	{
		name: "G",
		latin: "sol",
		color: "white",
	},
	{
		name: "G#",
		latin: "sol#",
		color: "black",
	},
	{
		name: "A",
		latin: "la",
		color: "white",
	},
	{
		name: "A#",
		latin: "la#",
		color: "black",
	},
	{
		name: "B",
		latin: "si",
		color: "white",
	},
];

/* https://en.wikipedia.org/wiki/Equal_temperament */
/* makes A4 = 440 Hz and then calculates other frequencies relative to this note. In this case, the loop runs from C-1 (MIDI note number 0) to G9 (MIDI note number 127), covering all 128 MIDI note numbers. */
function createNoteTable() {
	const A0_MIDI_NOTE = 57;
	const noteFreq = [];
	const notes = CHROMATIC_SCALE_NOTES.map((note) => note.name);
	const A4_FREQUENCY = 440; // in hertz
	const notesInChromaticScale = notes.length;

	for (let octave = 0; octave < notesInChromaticScale - 1; octave++) {
		noteFreq[octave] = [];
	}

	for (let n = 0; n < notesInChromaticScale * 11; n++) {
		const octave = Math.floor(n / notesInChromaticScale);
		const note = notes[n % notesInChromaticScale];
		const frequency =
			A4_FREQUENCY * Math.pow(2, (n - A0_MIDI_NOTE) / notesInChromaticScale);
		noteFreq[octave][note] = frequency;
	}

	return noteFreq;
}

const noteTable = createNoteTable();
const keyboardTable = {
	"en-us": {
		q: { name: "C", octave: 0 },
		2: { name: "C#", octave: 0 },
		w: { name: "D", octave: 0 },
		3: { name: "D#", octave: 0 },
		e: { name: "E", octave: 0 },
		r: { name: "F", octave: 0 },
		5: { name: "F#", octave: 0 },
		t: { name: "G", octave: 0 },
		6: { name: "G#", octave: 0 },
		y: { name: "A", octave: 0 },
		7: { name: "A#", octave: 0 },
		u: { name: "B", octave: 0 },
		i: { name: "C", octave: 1 },
		9: { name: "C#", octave: 1 },
		o: { name: "D", octave: 1 },
		0: { name: "D#", octave: 1 },
		p: { name: "E", octave: 1 },
		"[": { name: "F", octave: 1 },
		"=": { name: "F#", octave: 1 },
		"]": { name: "G", octave: 1 },
		a: { name: "G#", octave: 1 },
		z: { name: "A", octave: 1 },
		s: { name: "A#", octave: 1 },
		x: { name: "B", octave: 1 },
		c: { name: "C", octave: 2 },
		f: { name: "C#", octave: 2 },
		v: { name: "D", octave: 2 },
		g: { name: "D#", octave: 2 },
		b: { name: "E", octave: 2 },
		n: { name: "F", octave: 2 },
		j: { name: "F#", octave: 2 },
		m: { name: "G", octave: 2 },
		k: { name: "G#", octave: 2 },
		",": { name: "A", octave: 2 },
		l: { name: "A#", octave: 2 },
		".": { name: "B", octave: 2 },
		"/": { name: "C", octave: 3 },
	},
	"de-de": {
		q: { name: "C", octave: 0 },
		2: { name: "C#", octave: 0 },
		w: { name: "D", octave: 0 },
		3: { name: "D#", octave: 0 },
		e: { name: "E", octave: 0 },
		r: { name: "F", octave: 0 },
		5: { name: "F#", octave: 0 },
		t: { name: "G", octave: 0 },
		6: { name: "G#", octave: 0 },
		z: { name: "A", octave: 0 },
		7: { name: "A#", octave: 0 },
		u: { name: "B", octave: 0 },
		i: { name: "C", octave: 1 },
		9: { name: "C#", octave: 1 },
		o: { name: "D", octave: 1 },
		0: { name: "D#", octave: 1 },
		p: { name: "E", octave: 1 },
		ü: { name: "F", octave: 1 },
		ß: { name: "F#", octave: 1 },
		"+": { name: "G", octave: 1 },
		a: { name: "G#", octave: 1 },
		y: { name: "A", octave: 1 },
		s: { name: "A#", octave: 1 },
		x: { name: "B", octave: 1 },
		c: { name: "C", octave: 2 },
		f: { name: "C#", octave: 2 },
		v: { name: "D", octave: 2 },
		g: { name: "D#", octave: 2 },
		b: { name: "E", octave: 2 },
		n: { name: "F", octave: 2 },
		j: { name: "F#", octave: 2 },
		m: { name: "G", octave: 2 },
		k: { name: "G#", octave: 2 },
		",": { name: "A", octave: 2 },
		l: { name: "A#", octave: 2 },
		".": { name: "B", octave: 2 },
		"-": { name: "C", octave: 3 },
	},
};
export { CHROMATIC_SCALE_NOTES, createNoteTable, noteTable, keyboardTable };
