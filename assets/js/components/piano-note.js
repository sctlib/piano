export default class PianoNote extends HTMLElement {
	static get observedAttributes() {
		return ["show-info", "octave", "name"];
	}
	set octave(num) {
		this.setAttribute("octave", num);
	}
	get octave() {
		let num;
		try {
			num = Number(this.getAttribute("octave"));
		} catch (e) {}
		return num;
	}
	/* show info */
	set showInfo(bool) {
		this.setAttribute("show-info", bool.toString());
	}
	get showInfo() {
		return this.getAttribute("show-info") === "true";
	}
	/* note name */
	set name(str) {
		this.setAttribute("name", str);
	}
	get name() {
		return this.getAttribute("name");
	}
	/* note color, on the keyboard layout */
	set color(str) {
		this.setAttribute("color", str);
	}
	get color() {
		return this.getAttribute("color");
	}
	get noPointerEvents() {
		/* so clicks are disabled, and only the parent "piano-note" can emit */
		return "none";
	}
	attributeChangedCallback(attrName, oldVal, newVal) {
		this.render();
	}

	visualPlay({ velocity }) {
		this.setAttribute("is-playing", true);
		if (velocity) {
			this.setAttribute("velocity", velocity);
			this.style.setProperty("--note-velocity", velocity);
		}
	}
	visualStop() {
		this.removeAttribute("is-playing");
		this.removeAttribute("velocity");
		this.style.removeProperty("--note-velocity");
	}

	render() {
		this.innerHTML = "";
		const $key = document.createElement("piano-note-key");
		/* $key.style.pointerEvents = this.noPointerEvents; */
		this.append($key);
		if (this.showInfo) {
			const $info = document.createElement("piano-note-info");
			/* $info.style.pointerEvents = this.noPointerEvents; */
			$info.innerText = this.name;
			this.append($info);
		}
	}
}
