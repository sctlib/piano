import { CHROMATIC_SCALE_NOTES } from "../config.js";
import { MIDI_NOTES } from "../midi-gm.js";

export default class PianoOctave extends HTMLElement {
	static get observedAttributes() {
		return ["index", "show-info", "show-notes", "convention"];
	}
	/* octaves attr */
	set index(num) {
		this.setAttribute("index", num);
	}
	get index() {
		let num;
		try {
			num = Number(this.getAttribute("index"));
		} catch (e) {}
		return num;
	}
	/* show octaves */
	set showInfo(bool) {
		this.setAttribute("show-info", bool.toString());
	}
	get showInfo() {
		return this.getAttribute("show-info") === "true";
	}
	/* show octaves */
	set showNotes(bool) {
		this.setAttribute("show-notes", bool.toString());
	}
	get showNotes() {
		return this.getAttribute("show-notes") === "true";
	}
	/* note naming convention notation [english, (neo)latin] */
	set convention(str) {
		this.setAttribute("convention", str);
	}
	get convention() {
		return this.getAttribute("convention");
	}

	/* when an attribute changed */
	attributeChangedCallback(attrName, oldVal, newVal) {
		this.render();
	}
	connectedCallback() {}
	render() {
		this.innerHTML = "";
		const $octaveMain = document.createElement("piano-octave-main");

		CHROMATIC_SCALE_NOTES.forEach((note, noteIndex) => {
			const $note = document.createElement("piano-note");
			$note.setAttribute("octave", this.index);
			$note.setAttribute("color", note.color);
			$note.setAttribute("midi", MIDI_NOTES[note.name][this.index]);
			$note.setAttribute("index", noteIndex);
			if (this.convention === "latin") {
				$note.setAttribute("name", note.latin);
			} else {
				$note.setAttribute("name", note.name);
			}
			this.showNotes && $note.setAttribute("show-info", true);
			$octaveMain.append($note);
		});
		this.append($octaveMain);

		if (this.showInfo) {
			const $info = document.createElement("piano-octave-info");
			$info.innerText = this.index;
			this.append($info);
		}
	}
}
