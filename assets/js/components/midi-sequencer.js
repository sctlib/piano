export default class MidiSequencer extends HTMLElement {
	static get observedAttributes() {
		return ["bpm", "is-playing", "current-step", "tracks", "grid-size"];
	}

	set bpm(value) {
		value = Number(value);
		if (!isNaN(value) && value >= 1 && value <= 240) {
			this.setAttribute("bpm", value);
		} else {
			this.setAttribute("bpm", 60);
		}
	}

	get bpm() {
		let num = Number(this.getAttribute("bpm"));
		return isNaN(num) ? 60 : num;
	}

	set tracks(value) {
		this.setAttribute("tracks", JSON.stringify(value));
	}

	get tracks() {
		return JSON.parse(this.getAttribute("tracks")) || [];
	}

	get currentStep() {
		return parseInt(this.getAttribute("current-step")) || 0;
	}

	set currentStep(value) {
		this.setAttribute("current-step", value.toString());
	}

	get isPlaying() {
		return this.getAttribute("is-playing") === "true";
	}

	set isPlaying(bool) {
		if (bool) {
			this.setAttribute("is-playing", "true");
		} else {
			this.removeAttribute("is-playing");
		}
	}

	get gridSize() {
		let num = Number(this.getAttribute("grid-size"));
		return isNaN(num) ? 4 : num;
	}

	set gridSize(val) {
		val = Number(val);
		if (!isNaN(val) && val >= 1 && val <= 100) {
			this.setAttribute("grid-size", val);
		} else {
			this.setAttribute("grid-size", 4);
		}
	}

	attributeChangedCallback(attrName, oldVal, newVal) {
		if (attrName === "current-step" && this.isPlaying) {
			return;
		}
		if (attrName === "bpm" && this.isPlaying) {
			this.stopClock();
			this.startClock();
		}
		if (attrName === "is-playing") {
			if (newVal === "true") {
				if (!this.isPlaying) {
					this.currentStep = 0;
					this.startClock();
				}
			} else {
				this.stopClock();
			}
		}
		this.render();
	}

	constructor() {
		super();
		this.initializeDefaultProperties();
	}

	initializeDefaultProperties() {
		this.isPlaying = false;
		this.clockInterval = null;
		this.clockResolution = 24;
		this.currentStep = 0;
		this.currentTrackIndex = 0;
		this.bpm = 60;
		this.tracks = [];
		this.maxClipLength = 4;
		this.gridSize = 4;
	}

	connectedCallback() {
		this.render();
	}

	disconnectedCallback() {
		this.stopClock();
	}

	togglePlayback() {
		if (this.isPlaying) {
			this.stopClock();
		} else {
			this.currentStep = 0;
			this.startClock();
		}
		this.isPlaying = !this.isPlaying;
		this.render();
	}

	advanceStep() {
		const track = this.tracks[this.currentTrackIndex];
		if (track) {
			this.sendNote();
			this.currentStep = (this.currentStep + 1) % track.clip.notes.length;
		}
	}

	sendNote() {
		const track = this.tracks[this.currentTrackIndex];
		const note = track.clip.notes[this.currentStep];
		if (note && note.midi) {
			console.log(
				`Sending note ${note.midi} with duration ${note.durationInBeats}`
			);
		}
	}

	startClock() {
		this.currentStep = 0;
		const intervalDuration = (60 / this.bpm / this.clockResolution) * 1000;
		this.clockInterval = setInterval(
			() => this.advanceStep(),
			intervalDuration
		);
	}

	stopClock() {
		clearInterval(this.clockInterval);
	}

	handleTrackChange(event) {
		const {
			detail: newTrack,
			target: { index },
		} = event;
		const updatedTracks = [...this.tracks];
		updatedTracks[index] = newTrack;
		this.tracks = updatedTracks;
	}

	addTrack(track) {
		const updatedTracks = [...this.tracks];
		updatedTracks.push(track);
		this.tracks = updatedTracks;
	}

	exportSequencerProject() {
		const project = {
			name: `web_idea_${Date.now()}`,
			tempo: this.bpm,
			tracks: this.tracks,
		};
		return JSON.stringify(project);
	}

	importSequencerProject(json) {
		const { tempo, tracks } = JSON.parse(json) || {};
		if (tempo) {
			this.bpm = tempo;
		}
		if (tracks) {
			this.tracks = tracks;
		}
	}

	onTrackAdd(event) {
		this.addTrack(event.detail.track);
	}

	onTrackUpdate(event) {
		const {
			detail: newTrack,
			target: { index },
		} = event;
		const updatedTracks = [...this.tracks];
		updatedTracks[index] = newTrack;
		this.tracks = updatedTracks;
	}

	onTrackDelete(event) {
		const {
			detail: { index },
		} = event;
		const updatedTracks = [...this.tracks];
		updatedTracks.splice(index, 1);
		this.tracks = updatedTracks;
	}

	onPlay(event) {
		this.togglePlayback();
	}

	onExport() {
		const projectJson = this.exportSequencerProject();
		const filename = `sequencer_project_${Date.now()}.json`;
		const blob = new Blob([projectJson], { type: "application/json" });
		const url = URL.createObjectURL(blob);
		const link = document.createElement("a");
		link.href = url;
		link.download = filename;
		link.click();
	}

	onImport(event) {
		const file = event.target.files[0];
		const reader = new FileReader();
		reader.onload = (e) => {
			const contents = e.target.result;
			this.importSequencerProject(contents);
		};
		reader.readAsText(file);
	}

	onSubmit(event) {
		event.preventDefault();
	}

	onInput(event) {
		const { name, value } = event.target;
		if (name === "bpm") {
			this.bpm = parseInt(value);
		} else if (name === "gridSize") {
			this.gridSize = parseInt(value);
		}
	}

	render() {
		this.innerHTML = "";
		const menu = this.createMenu();
		const tracks = this.createTracks();
		const tracksContainer = document.createElement("ol");
		tracksContainer.style.setProperty("--grid-size", this.gridSize);

		tracksContainer.append(...tracks);
		this.append(menu, tracksContainer);
	}

	createMenu() {
		const menu = document.createElement("form");
		menu.addEventListener("submit", this.onSubmit.bind(this));

		const playBtn = document.createElement("button");
		playBtn.innerText = this.isPlaying ? "Stop" : "Play";
		playBtn.addEventListener("click", this.onPlay.bind(this));

		const addTrackBtn = document.createElement("button");
		addTrackBtn.innerText = "Add Track";
		addTrackBtn.addEventListener("click", this.onTrackAdd.bind(this));

		const exportBtn = document.createElement("button");
		exportBtn.innerText = "Export";
		exportBtn.addEventListener("click", this.onExport.bind(this));

		const importLabel = document.createElement("label");
		importLabel.innerText = "Import";
		const importInput = document.createElement("input");
		importInput.setAttribute("title", "Import project");
		importInput.setAttribute("placeholder", "Import project");
		importInput.setAttribute("type", "file");
		importInput.setAttribute("accept", ".json");
		importInput.addEventListener("change", this.onImport.bind(this));
		importLabel.append(importInput);

		const bpmInput = document.createElement("input");
		bpmInput.setAttribute("name", "bpm");
		bpmInput.setAttribute("type", "number");
		bpmInput.setAttribute("step", 1);
		bpmInput.setAttribute("min", 1);
		bpmInput.setAttribute("max", 240);
		bpmInput.value = this.bpm;
		bpmInput.addEventListener("change", this.onInput.bind(this));

		const gridSizeInput = document.createElement("input");
		gridSizeInput.setAttribute("name", "gridSize");
		gridSizeInput.setAttribute("type", "number");
		gridSizeInput.setAttribute("min", 1);
		gridSizeInput.setAttribute("max", 100);
		gridSizeInput.value = this.gridSize;
		gridSizeInput.addEventListener("change", this.onInput.bind(this));

		menu.append(
			playBtn,
			addTrackBtn,
			exportBtn,
			importLabel,
			bpmInput,
			gridSizeInput
		);
		return menu;
	}

	createTracks() {
		return this.tracks.map((track, index) => this.createTrack(track, index));
	}

	createTrack(track, index) {
		const deleteButton = document.createElement("button");
		deleteButton.innerText = "Delete Track";
		deleteButton.addEventListener("click", this.onTrackDelete.bind(this));

		const midiTrack = document.createElement("midi-track");
		midiTrack.track = track;
		midiTrack.setAttribute("index", index);
		midiTrack.setAttribute("grid-size", this.gridSize);
		midiTrack.addEventListener("trackUpdate", this.onTrackUpdate.bind(this));

		const trackContainer = document.createElement("li");
		trackContainer.append(midiTrack, deleteButton);

		return trackContainer;
	}
}
