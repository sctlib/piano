class PianoApp extends HTMLElement {
	connectedCallback() {
		this.$pianoApp = document.createElement("piano-vst");
		this.$details = document.createElement("details");
		this.$summary = document.createElement("summary");
		this.$fullscreen = this.createFullscreen();
		this.render();
	}
	createFullscreen() {
		const $fullscreen = document.createElement("button");
		$fullscreen.title = `Fullscreen`;
		$fullscreen.addEventListener("click", this.onToggleFullscreen.bind(this));
		$fullscreen.innerText = "⇱";
		return $fullscreen;
	}
	render() {
		this.innerHTML = "";
		this.$summary.innerText = "🎹";
		this.$summary.title = "Piano";
		this.$summary.append(this.$fullscreen);
		this.$details.append(this.$summary);
		this.$details.append(this.$pianoApp);
		this.append(this.$details);
	}
	onToggleFullscreen() {
		if (window.fullScreen) {
			document.exitFullscreen();
		} else {
			this.setAttribute("fullscreen", "true");
			this.$details.setAttribute("open", "");
			this.requestFullscreen();
		}
	}
}

export default PianoApp;
