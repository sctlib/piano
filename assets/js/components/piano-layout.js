import { keyboardTable, CHROMATIC_SCALE_NOTES } from "../config.js";

import {
	MIDI_NOTES,
	MIDI_MESSAGES,
	MIDI_CONTROL_CHANGES,
	MIDI_NOTES_MAP,
	getMidimessageInfo,
	getMidiProgram,
	getPercussionFromProgram,
} from "../midi-gm.js";

// displays a piano-layout
export default class PianoLayout extends HTMLElement {
	static get observedAttributes() {
		return [
			"root-octave", // starting from the "root-octave"
			"octaves", // for as low as many "octaves",
			"max-octave", // from 1 to "max-octave" (9)
			"show-octaves", // it will "show-octave" index
			"show-notes", // it will "show-notes" names
			"convention", // using the specified notation "convention"
			"modal-shift", // root key + N to play N semi-tone above
			"show-input-keyboard", // and display "keyboard inputs"
			"keyboard-language", // using the browser language's for keyboard layout
			"input-keyboard", // if "input-keyboard" is activated
			"input-mouse", // also for "input-mouse"
			"input-midi", // and the incoming "input-midi"
			"input-midi-clock", // does it receive midi clock in
			"output-midi", // and can also send "output-midi"
		];
	}
	set octaves(num) {
		this.setAttribute("octaves", num);
	}
	get octaves() {
		let num;
		try {
			num = Number(this.getAttribute("octaves")) || 1;
		} catch (e) {}
		return num;
	}
	set rootOctave(num) {
		this.setAttribute("root-octave", num);
	}
	get rootOctave() {
		let num;
		try {
			num = Number(this.getAttribute("root-octave")) || 1;
		} catch (e) {}
		return num;
	}
	get maxOctave() {
		let num;
		try {
			num = Number(this.getAttribute("max-octave")) || 9;
		} catch (e) {}
		return num;
	}
	set showOctaves(bool) {
		this.setAttribute("show-octaves", bool.toString());
	}
	get showOctaves() {
		return this.getAttribute("show-octaves") === "true";
	}
	get outputMidi() {
		return this.getAttribute("output-midi") === "true";
	}
	get inputMidi() {
		return this.getAttribute("input-midi") === "true";
	}
	get inputMidiClock() {
		return this.getAttribute("input-midi-clock") === "true";
	}
	get inputMouse() {
		return this.getAttribute("input-mouse") === "true";
	}
	get inputKeyboard() {
		return this.getAttribute("input-keyboard") === "true";
	}
	get keyboardLanguage() {
		return (
			this.getAttribute("keyboard-language") ||
			navigator.language.toLowerCase() ||
			"en-us"
		);
	}
	get showInputKeyboard() {
		return this.getAttribute("show-input-keyboard") === "true";
	}
	set showNotes(bool) {
		this.setAttribute("show-notes", bool.toString());
	}
	get showNotes() {
		return this.getAttribute("show-notes") === "true";
	}
	set convention(str) {
		this.setAttribute("convention", str);
	}
	get convention() {
		const convAttr = this.getAttribute("convention");
		const conventions = ["english", "latin"];
		const convFound = conventions.includes(convAttr);
		return convAttr || conventions[0];
	}

	set modalShift(num) {
		this.setAttribute("modal-shift", num);
	}
	get modalShift() {
		let num;
		try {
			num = Number(this.getAttribute("modal-shift")) || 0;
		} catch (e) {}
		return num;
	}

	get userInputRequested() {
		return this.inputKeyboard || this.inputMidi || this.inputMouse;
	}

	attributeChangedCallback() {
		this.render();
	}
	constructor() {
		super();
		this._userInputListeners = [];
		this.keyboardTable = null;
		this.midi = null;
		this.midiNotes = {};
		this.pushdown = false;
		this.inputListenersMap = {
			keyboard: {
				keydown: this.onKeydown,
				keyup: this.onKeyup,
			},
			mouse: {
				mousedown: this.onPushdown,
				mousemove: this.onPushdrag,
				mouseup: this.onPushup,
				mouseleave: this.onPushup,
				touchstart: this.onPushdown,
				touchmove: this.onPushdrag,
				touchend: this.onPushup,
			},
		};
	}
	async connectedCallback() {
		if (this.userInputRequested) {
			this.setAttribute("tabindex", 0);
		}
		if (this.inputKeyboard) {
			this.keyboardTable = keyboardTable[this.keyboardLanguage];
		}
		/* for input keyboard & mouse, only listen when focused */
		if (this.inputKeyboard || this.inputMouse) {
			this.focusListener = this.handleFocus.bind(this);
			this.addEventListener("blur", this.focusListener.bind(this));
			this.addEventListener("focus", this.focusListener.bind(this));
		}
		/* for input/output midi, listen/send even when not focused */
		if (this.inputMidi || this.outputMidi) {
			this.midiOutput = null;
			this.midiInput = null;
			try {
				this.midi = await navigator.requestMIDIAccess({ sysex: false });
				this.midi.addEventListener(
					"statechange",
					this.onMidiStatechange.bind(this)
				);
				/* for (const input of midi.inputs.values()) {
					 input.onmidimessage = this.onMidimessage;
					 } */
			} catch (e) {
				console.info("Could not get MIDI input authorization", e);
			}
		}
		this.render();
	}
	disconnectedCallback() {
		if (this.inputKeyboard || this.inputMouse) {
			this.removeEventListeners();
			this.removeEventListener("blur", this.focusListener);
			this.removeEventListener("focus", this.focusListener);
		}
	}
	handleFocus(e) {
		if (e.type === "focus") {
			this.addEventListeners();
			this.setAttribute("input-active", true);
		}
		if (e.type === "blur") {
			this.removeEventListeners();
			this.removeAttribute("input-active");
		}
	}
	async addEventListeners() {
		const { keyboard, mouse, midi } = this.inputListenersMap;
		if (this.inputKeyboard) {
			Object.entries(keyboard).forEach(this.addListenerEntry.bind(this));
		}
		/* if (this.inputMidi) {
			 Object.entries(midi).forEach(this.addListenerEntry.bind(this));
			 } */
		if (this.inputMouse) {
			Object.entries(mouse).forEach(this.addListenerEntry.bind(this));
		}
	}
	removeEventListeners() {
		if (this._userInputListeners && this._userInputListeners.length) {
			this._userInputListeners.forEach(([event, listener]) => {
				this.removeEventListener(event, listener);
			});
			this._userInputListeners = [];
		}
		if (this.midi && this.midi.inputs) {
			this.midi.inputs.forEach((midiInput) => {
				// should remove midi channels ?
			});
		}
	}
	addListenerEntry(inputListener) {
		const [event, handler] = inputListener;
		const listener = handler.bind(this);
		this.addEventListener(event, listener);
		this._userInputListeners.push([event, listener]);
	}
	onMidiStatechange(event) {
		console.info("onMidiStatechange", event);
	}
	onMidimessage({ data }) {
		// Get the appropriate message type handler and execute it
		const midiInfo = getMidimessageInfo(data);
		if (MIDI_MESSAGES.clock === midiInfo.type) return;
		const messageHandlers = {
			[MIDI_MESSAGES.clock]: this.onMidiClock.bind(this),
			[MIDI_MESSAGES.noteOn]: this.onMidiNoteOnOrOffMessage.bind(this),
			[MIDI_MESSAGES.noteOff]: this.onMidiNoteOffMessage.bind(this),
			[MIDI_MESSAGES.programChange]: this.onMidiProgramChange.bind(this),
			[MIDI_MESSAGES.pitchBend]: this.onMidiPitchBend.bind(this),
			[MIDI_MESSAGES.polyAftertouch]: this.onMidiPolyAftertouch.bind(this),
			[MIDI_MESSAGES.channelAftertouch]: this.onMidiAftertouch.bind(this),
			[MIDI_MESSAGES.controlChange]: this.onMidiControlChange.bind(this),
		};
		const messageHandler = messageHandlers[midiInfo.type];
		if (messageHandler) {
			return messageHandler(midiInfo);
		}
	}
	onMidiNoteOnOrOffMessage(midiInfo) {
		return midiInfo.velocity > 0
			? this.onMidiNoteOnMessage(midiInfo)
			: this.onMidiNoteOffMessage(midiInfo);
	}
	onMidiNoteOnMessage({ command, channel, dataByte1: midiNote, velocity }) {
		const note = this.getMidiNoteInfo(midiNote);
		note.channel = channel;
		note.velocity = velocity;
		this.playNote(note);
	}

	onMidiNoteOffMessage({ command, channel, dataByte1: midiNote, velocity }) {
		const note = this.getMidiNoteInfo(midiNote);
		this.stopNote(note);
	}

	onMidiSustain(midiInfo) {
		return midiInfo.velocity > 63
			? this.onMidiSustainOn(midiInfo)
			: this.onMidiSustainOff(midiInfo);
	}

	onMidiSustainOn(midiInfo) {
		this.dispatchEvent(
			new CustomEvent("sustainon", {
				bubbles: true,
				detail: midiInfo,
			})
		);
	}
	onMidiSustainOff(midiInfo) {
		this.dispatchEvent(
			new CustomEvent("sustainoff", {
				bubbles: true,
				detail: midiInfo,
			})
		);
	}
	onMidiModulation(midiInfo) {
		this.dispatchEvent(
			new CustomEvent("modulation", {
				bubbles: true,
				detail: midiInfo,
			})
		);
	}

	onMidiVolumeChange(midiInfo) {
		this.dispatchEvent(
			new CustomEvent("volumechange", {
				bubbles: true,
				detail: midiInfo,
			})
		);
	}
	onMidiClock(midimessage) {
		/* if (!this.inputMidiClock) return; */
	}
	onMidiAftertouch(midimessage) {
		console.log(midimessage);
		this.dispatchEvent(
			new CustomEvent("aftertouch", {
				bubbles: true,
				detail: midimessage,
			})
		);
	}
	onMidiPolyAftertouch(midimessage) {
		console.log(midimessage);
		this.dispatchEvent(
			new CustomEvent("polyaftertouch", {
				bubbles: true,
				detail: midimessage,
			})
		);
	}
	onMidiPitchBend(midimessage) {
		console.log(midimessage);
		this.dispatchEvent(
			new CustomEvent("pitchben", {
				bubbles: true,
				detail: midimessage,
			})
		);
	}
	onMidiProgramChange(midimessage) {
		const { dataByte1: program, channel } = midimessage;
		const instrument = getMidiProgram(program, channel);
		this.dispatchEvent(
			new CustomEvent("programchange", {
				bubbles: true,
				detail: midimessage,
			})
		);
	}
	onMidiControlChange(midimessage) {
		console.log("control change", midimessage);
		const midiInfo = getMidimessageInfo(midimessage);
		const MCC = MIDI_CONTROL_CHANGES;
		const controlChangeHandlers = {
			[MCC.modulation]: this.onMidiModulation.bind(this),
			/* [MCC.volume]: this.onMidiVolumeChange.bind(this), */
			/* [MCC.channelPan]: this.onMidiChannelPan.bind(this), */
			/* [MCC.expressionController]: this.onMidiExpressionController.bind(this), */
			/* [MCC.ribbonController]: this.onMidiRibbonController.bind(this), */
			[MCC.Sustain]: this.onMidiSustain.bind(this),
			/* [MCC.sustainLevel]: this.onMidiSustainLevel.bind(this), */
			/* [MCC.decayGeneric]: this.onMidiDecayGeneric.bind(this), */
			/* [MCC.lsb]: this.onMidiRegisteredParameterNumberLSB.bind(this), */
			/* [MCC.msb]: this.onMidiRegisteredParameterNumberMSB.bind(this), */
			/* [MCC.resetAllControllers]: this.onMidiResetAllControllers.bind(this), */
			/* [MCC.allNotes]: this.onMidiAllNotes.bind(this), */
		};
		const controlChangeHandler = controlChangeHandlers[midiInfo.type];
		if (controlChangeHandler) {
			return controlChangeHandler(midiInfo);
		}
		this.dispatchEvent(
			new CustomEvent("controlchange", {
				bubbles: true,
				detail: midimessage,
			})
		);
	}
	onMidiChannelPan(midiInfo) {
		this.dispatchEvent(
			new CustomEvent("channelpan", {
				bubbles: true,
				detail: midimessage,
			})
		);
	}
	onMidiDecayGeneric(midiInfo) {
		this.dispatchEvent(
			new CustomEvent("decaygeneric", {
				bubbles: true,
				detail: midimessage,
			})
		);
	}

	onKeydown(event) {
		if (
			["OS", "Control", "Alt"].includes(event.key) ||
			event.ctrlKey ||
			event.altKey
		) {
			return;
		}
		const { key, shiftKey } = event;
		const isPianoKey = this.keyIsPianoKey(key);
		if (isPianoKey) {
			event.preventDefault();
			const keyboardNote = this.getKeyboardNoteInfo(key);
			if (!keyboardNote) return;
			const note = this.getKeyboardNoteMidiInfo(keyboardNote);
			if (note && !event.repeat) {
				this.playNote(note);
			}
		} else if (key === "ArrowLeft" || key === "ArrowRight") {
			event.preventDefault();
			if (shiftKey) {
				this.modalShiftRoot(event);
			} else {
				this.shiftOctaves(event);
			}
		} else if (key === "ArrowUp" || key === "ArrowDown") {
			event.preventDefault();
			this.moveRootOctave(event);
		}
	}
	onKeyup(event) {
		const key = event.key;
		const isPianoKey = this.keyIsPianoKey(key);
		if (!isPianoKey) return;
		const keyboardNote = this.getKeyboardNoteInfo(key);
		if (!keyboardNote) return;
		const note = this.getKeyboardNoteMidiInfo(keyboardNote);
		if (note) {
			this.stopNote(note);
		}
	}
	onPushdown(event) {
		this.pushdown = true;
		const $pianoNote = this.getNoteDomfromTarget(event.target);
		if (!$pianoNote) return;
		event.preventDefault();
		const note = this.getDomNoteInfo($pianoNote);
		if (this.midiNotes[note.midi]) return;
		this.playNote(note);
	}
	onPushdrag(event) {
		if (!this.pushdown) return;
		let $pianoNote;
		if (event.touches && event.touches.length) {
			const touch = event.touches[0];
			const targetElement = document.elementFromPoint(
				touch.clientX,
				touch.clientY
			);
			$pianoNote = this.getNoteDomfromTarget(targetElement);
		} else {
			$pianoNote = this.getNoteDomfromTarget(event.target);
		}
		if (!$pianoNote) return;
		event.preventDefault();
		const note = this.getDomNoteInfo($pianoNote);
		if (this.midiNotes[note.midi]) return;
		this.playNote(note);
	}
	onPushup({ target }) {
		this.pushdown = false;
		const $pianoNote = this.getNoteDomfromTarget(target);
		if ($pianoNote) {
			const note = this.getDomNoteInfo($pianoNote);
			this.stopNote(note);
		}
		Object.keys(this.midiNotes).forEach((midiNote) => {
			let note;
			if (this.modalShift) {
				note = this.getMidiNoteInfo(midiNote - 1);
			} else {
				note = this.getMidiNoteInfo(midiNote);
			}
			this.stopNote(note);
		});
	}
	async onMidiInputSelect(event) {
		const { value } = event.target;
		if (this.midiInput) {
			this.midiInput.removeEventListener(
				"midimessage",
				this.onMidimessage.bind(this)
			);
			await this.midiInput.close();
			this.midiInput = null;
		}
		if (!this.midiInput) {
			this.midiInput = this.midi.inputs.get(value);
			this.midiInput.addEventListener(
				"midimessage",
				this.onMidimessage.bind(this)
			);
		}
	}
	onMidiOutputSelect(event) {
		const { value } = event.target;
		console.log("midi output", value);
		this.midiOutput = this.midi.outputs.get(value);
	}
	getMidiNoteInfo(midiNote) {
		return MIDI_NOTES_MAP[midiNote];
	}
	getKeyboardNoteMidiInfo(keyboardNote) {
		const { octave, name } = keyboardNote;
		const actualOctave = octave + this.rootOctave;
		return {
			name,
			octave: actualOctave,
			midi: MIDI_NOTES[name][actualOctave],
		};
	}
	getKeyboardNoteInfo(key) {
		/* otherwise we will mutate the keyboardTable object itself */
		const keyInfo = this.keyboardTable[key];
		if (keyInfo) {
			return { ...keyInfo };
		}
	}
	keyIsPianoKey(key) {
		return !!this.keyboardTable[key];
	}
	getDomNoteInfo($pianoNote) {
		const attrs = ["octave", "midi", "name"];
		const info = {};
		attrs.forEach((attrName) => {
			const attrVal = $pianoNote.getAttribute(attrName);
			if (attrName === "name") {
				info["name"] = attrVal;
			} else if (["octave", "midi"].includes(attrName)) {
				try {
					const attrValAsNum = Number(attrVal);
					info[attrName] = attrValAsNum;
				} catch (e) {}
			} else {
				info[attrName] = attrVal;
			}
		});
		return info;
	}
	getNoteDomfromTarget(target) {
		let $pianoNote;
		if (this.isDomPianoNote(target)) {
			$pianoNote = target;
		} else if (this.isDomPianoNote(target.parentNode)) {
			$pianoNote = target.parentNode;
		}
		return $pianoNote;
	}
	isDomPianoNote($domElement) {
		if (!$domElement) return;
		return $domElement.nodeName === "PIANO-NOTE";
	}
	stopNote(noteData) {
		let note;
		if (this.modalShift) {
			const modalNote = this.getMidiNoteInfo(noteData.midi + this.modalShift);
			note = modalNote;
		} else {
			note = noteData;
		}
		this.midiStopNote(note);
		this.visualStopNote(note);
		delete this.midiNotes[note.midi];
		this.dispatchEvent(
			new CustomEvent("noteoff", {
				bubbles: true,
				detail: note,
			})
		);
	}
	playNote(noteData) {
		/* some note can have that */
		const { channel, velocity } = noteData;
		let newNote;
		if (this.modalShift) {
			const modalNote = this.getMidiNoteInfo(noteData.midi + this.modalShift);
			newNote = modalNote;
		} else {
			newNote = noteData;
		}
		if (channel) {
			newNote.channel = channel;
		}
		if (velocity) {
			newNote.velocity = velocity;
		}
		const { name, octave, midi } = newNote;
		const isPlaying = this.midiNotes[midi];
		if (isPlaying) return;
		this.midiNotes[midi] = true;
		this.midiPlayNote({ midi });
		this.visualPlayNote({ name, octave, midi, velocity, channel });
		this.dispatchEvent(
			new CustomEvent("noteon", {
				bubbles: true,
				detail: newNote,
			})
		);
	}
	visualPlayNote(note) {
		const $note = this.querySelectorNote(note);
		if ($note && typeof $note.visualPlay === "function") {
			$note.visualPlay(note);
		}
	}
	visualStopNote(note) {
		const $note = this.querySelectorNote(note);
		if ($note && typeof $note.visualStop === "function") {
			$note.visualStop(note);
		}
	}
	querySelectorNote({ octave, name, midi }) {
		if (midi) {
			return this.querySelector(`piano-note[midi="${midi}"]`);
		} else if (octave && name) {
			return this.querySelector(
				`piano-note[octave="${octave}"][name="${name}"]`
			);
		}
	}
	midiPlayNote({ channel, midi, velocity = 0x7f }) {
		if (this.midiOutput) {
			const noteOnMessage = [MIDI_MESSAGES.noteOn, midi, velocity];
			this.midiOutput.send(noteOnMessage);
		}
	}
	midiStopNote({ midi }) {
		if (this.midiOutput) {
			const noteOffMessage = [MIDI_MESSAGES.noteOff, midi, 0x00];
			this.midiOutput.send(noteOffMessage);
		}
	}
	modalShiftRoot(event) {
		if (event.key === "ArrowLeft") {
			this.modalShift = Math.max(0, this.modalShift - 1);
		} else if (event.key === "ArrowRight") {
			this.modalShift = Math.min(
				this.modalShift + 1,
				CHROMATIC_SCALE_NOTES.length
			);
		}
	}
	shiftOctaves(event) {
		if (event.key === "ArrowLeft") {
			this.octaves = Math.max(1, this.octaves - 1);
		} else if (event.key === "ArrowRight") {
			this.octaves = Math.min(this.octaves + 1, this.maxOctave);
		}
	}
	moveRootOctave(event) {
		if (event.key === "ArrowDown") {
			this.rootOctave = Math.max(0, this.rootOctave - 1);
		} else if (event.key === "ArrowUp") {
			this.rootOctave = Math.min(
				this.maxOctave - this.octaves,
				this.rootOctave + 1
			);
		}
	}
	render() {
		this.innerHTML = "";
		Array(this.octaves)
			.fill(0)
			.forEach((_, index) => {
				const $octave = this.createOctaveElement(index);
				this.append($octave);
			});
		if (this.midi && (this.outputMidi || this.inputMidi)) {
			const $settings = document.createElement("piano-settings");
			this.outputMidi && $settings.append(this.createSelectMidiOutputElement());
			this.inputMidi && $settings.append(this.createSelectMidiInputElement());
			this.append($settings);
		}
	}
	createOctaveElement(index) {
		const $octave = document.createElement("piano-octave");
		$octave.setAttribute("index", index + this.rootOctave);
		this.showOctaves && $octave.setAttribute("show-info", true);
		this.showNotes && $octave.setAttribute("show-notes", true);
		this.convention && $octave.setAttribute("convention", this.convention);
		return $octave;
	}
	createSelectMidiOutputElement() {
		if (!this.midi || !this.midi.outputs.size) return;
		const rootText = "midi-out";
		const $outputSelector = document.createElement("select");
		this.midi.outputs.forEach((output) => {
			let $option = document.createElement("option");
			$option.value = output.id;
			$option.text = `${output.name}@${rootText}`;
			$outputSelector.append($option);
		});
		const $defaultOption = document.createElement("option");
		$defaultOption.text = rootText;
		$defaultOption.disabled = true;
		$defaultOption.selected = true;
		$outputSelector.prepend($defaultOption);
		$outputSelector.addEventListener(
			"change",
			this.onMidiOutputSelect.bind(this)
		);
		return $outputSelector;
	}
	createSelectMidiInputElement() {
		if (!this.midi || !this.midi.inputs.size) return;
		const rootText = "midi-in";
		const $inputSelector = document.createElement("select");
		this.midi.inputs.forEach((input) => {
			let $option = document.createElement("option");
			$option.value = input.id;
			$option.text = `${input.name}@${rootText}`;
			$inputSelector.append($option);
		});
		const $defaultOption = document.createElement("option");
		$defaultOption.text = rootText;
		$defaultOption.disabled = true;
		$defaultOption.selected = true;
		$inputSelector.prepend($defaultOption);
		$inputSelector.addEventListener(
			"change",
			this.onMidiInputSelect.bind(this)
		);
		return $inputSelector;
	}
}
