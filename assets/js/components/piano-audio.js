import { noteTable } from "../config.js";
import { MIDI_NOTES_MAP } from "../midi-gm.js";
import {
	Synth,
	AudioSynth,
	AudioSynthInstrument,
} from "../../libs/audiosynth.js";

export default class PianoAudio extends HTMLElement {
	get waveforms() {
		return ["triangle", "square", "sine", "sawtooth"];
	}

	get MAX_GAIN() {
		return 0.5;
	}

	get sustainPedalActive() {
		return Object.keys(this.heldMidiNotes).length > 0;
	}

	constructor() {
		super();
		this.midiNotes = {};
		this.heldMidiNotes = {};
		this.midiOscillators = {};
		this.audioOscillators = {};
		this.adsr = {
			attack: 10,
			decay: 400,
			sustain: 600,
			release: 300,
		};
	}

	connectedCallback() {
		const INITIAL_VOLUME = 0.2;

		this.audioContext = new (window.AudioContext ||
			window.webkitAudioContext)();
		this.oscList = [];
		this.noteTable = noteTable;
		this.oscillatorType = this.waveforms[0];

		this.mainGainNode = this.audioContext.createGain();
		this.mainGainNode.connect(this.audioContext.destination);
		this.mainGainNode.gain.value = INITIAL_VOLUME;
		Synth.setVolume(INITIAL_VOLUME);

		const piano = Synth.createInstrument("piano");
		const organ = Synth.createInstrument("organ");
		const organ2 = Synth.createInstrument("organ2");
		const acoustic = Synth.createInstrument("acoustic");
		const edm = Synth.createInstrument("edm");

		this.audioSynthInstruments = [piano, organ, organ2, acoustic, edm];
		this.audioSynthInstrumentNames = this.audioSynthInstruments.map(
			(instru) => instru.name
		);
		this.render();
	}

	getMidiNoteInfo(midiNote) {
		return MIDI_NOTES_MAP[midiNote];
	}

	sustainOn() {
		console.log("audio sustain on");
		this.heldMidiNotes = { ...this.midiNotes };
		console.log(this.heldMidiNotes);
	}
	sustainOff() {
		console.log("audio sustain off");
		Object.keys(this.heldMidiNotes).forEach((note) => {
			this.stopNote({ midi: note });
		});
		this.heldMidiNotes = {};
	}

	playNote({ octave, name, midi }) {
		const { sustain } = this.adsr;
		if (this.waveforms.includes(this.oscillatorType)) {
			const octaveData = this.noteTable[octave];
			if (!octaveData) return;
			const noteFrequence = octaveData[name];
			if (noteFrequence) {
				const oscillator = this.playTone(noteFrequence);
				this.midiOscillators[midi] = oscillator;
			}
		}
		if (this.audioSynthInstrumentNames.includes(this.oscillatorType)) {
			this.playInstrument({ octave, name, midi });
		}
		this.midiNotes[midi] = true;
		if (this.sustainPedalActive) {
			this.heldMidiNotes[midi] = true;
		}
	}

	stopNote(note) {
		if (!note.octave || !note.name) {
			note = this.getMidiNoteInfo(note.midi);
		}
		if (this.heldMidiNotes[note.midi]) return;

		if (this.waveforms.includes(this.oscillatorType)) {
			this.stopTone(note);
		}

		if (this.audioSynthInstrumentNames.includes(this.oscillatorType)) {
			this.stopInstrument(note);
		}
		if (this.heldMidiNotes[note.midi]) {
			delete this.heldMidiNotes[note.midi];
			return;
		}
		delete this.midiNotes[note.midi];
	}

	stopTone({ midi }) {
		if (!this.midiOscillators[midi]) return;
		const { oscillator, gainNode } = this.midiOscillators[midi];
		if (!oscillator) return;

		const { release } = this.adsr;
		const currentTime = this.audioContext.currentTime;

		gainNode.gain.cancelScheduledValues(currentTime);
		gainNode.gain.setValueAtTime(gainNode.gain.value, currentTime);
		gainNode.gain.linearRampToValueAtTime(0, currentTime + release / 1000); // release

		oscillator.stop(currentTime + release / 1000);
		delete this.midiOscillators[midi];
	}
	stopInstrument({ midi }) {
		if (!this.audioOscillators[midi]) return;
		const { gainNode, source } = this.audioOscillators[midi];
		if (gainNode) {
			const { release } = this.adsr;
			const currentTime = this.audioContext.currentTime;

			gainNode.gain.cancelScheduledValues(currentTime);
			gainNode.gain.linearRampToValueAtTime(0, currentTime + release / 1000);

			source.stop(currentTime + release / 1000);
		}

		delete this.audioOscillators[midi];
	}
	playTone(freq) {
		let osc = this.audioContext.createOscillator();
		osc.type = this.oscillatorType;
		osc.frequency.value = freq;

		const gainNode = this.audioContext.createGain();
		gainNode.gain.value = 0;
		osc.connect(gainNode);
		gainNode.connect(this.mainGainNode);

		const { attack, decay, sustain, release } = this.adsr;
		const currentTime = this.audioContext.currentTime;

		gainNode.gain.setValueAtTime(0, currentTime);
		gainNode.gain.linearRampToValueAtTime(
			this.MAX_GAIN,
			currentTime + attack / 1000
		);
		gainNode.gain.linearRampToValueAtTime(
			this.MAX_GAIN * (sustain / 1000),
			currentTime + attack / 1000 + decay / 1000
		);
		gainNode.gain.linearRampToValueAtTime(
			0,
			currentTime +
				attack / 1000 +
				decay / 1000 +
				sustain / 1000 +
				release / 1000
		);

		osc.start();

		return { oscillator: osc, gainNode: gainNode };
	}

	async playInstrument({ name, octave, midi }) {
		const { attack, decay, sustain, release } = this.adsr;
		const instrumentIndex = this.audioSynthInstrumentNames.indexOf(
			this.oscillatorType
		);
		const instrument = this.audioSynthInstruments[instrumentIndex];

		// Generate a base64 audio file with the instrument
		const audioData = instrument.play(name, octave, release / 1000);

		// Convert the base64 audio file to an array buffer
		const response = await fetch(audioData);
		const arrayBuffer = await response.arrayBuffer();

		// Decode the array buffer into an audio buffer
		const audioBuffer = await this.audioContext.decodeAudioData(arrayBuffer);

		// Create an audio buffer source node
		const source = this.audioContext.createBufferSource();
		source.buffer = audioBuffer;

		const gainNode = this.audioContext.createGain();
		gainNode.gain.value = 0;
		source.connect(gainNode);
		gainNode.connect(this.audioContext.destination);
		source.start();

		const currentTime = this.audioContext.currentTime;
		gainNode.gain.linearRampToValueAtTime(
			this.MAX_GAIN,
			currentTime + attack / 1000
		);
		gainNode.gain.linearRampToValueAtTime(
			this.MAX_GAIN * (sustain / 1000),
			currentTime + attack / 1000 + decay / 1000
		);
		gainNode.gain.linearRampToValueAtTime(
			0,
			currentTime +
				attack / 1000 +
				decay / 1000 +
				sustain / 1000 +
				release / 1000
		);

		this.audioOscillators[midi] = { source, gainNode };
	}

	onADSR({ target }) {
		const { name, value } = target;
		const param = event.target.name;
		this.adsr[param] = Number(event.target.value);
	}

	render() {
		// Create the waveform selector
		const $waveformSelector = this.createWaveformSelect();
		this.appendChild($waveformSelector);

		// Create the ADSR input elements
		const $adsrInputs = this.createADSRInput();
		this.appendChild($adsrInputs);
	}

	createWaveformSelect() {
		const $select = document.createElement("select");
		[...this.waveforms, ...this.audioSynthInstrumentNames].forEach((type) => {
			let option = document.createElement("option");
			option.value = type;
			option.text = type;
			if (type === "sine") option.selected = true;
			$select.add(option);
		});
		let defaultOption = document.createElement("option");
		defaultOption.text = "sound";
		defaultOption.disabled = true;
		defaultOption.selected = true;
		$select.prepend(defaultOption);

		// Update the oscillator type when the selected option changes
		$select.addEventListener("change", (event) => {
			const { value } = event.target;
			this.oscillatorType = value;
		});
		return $select;
	}

	/* (ADSR) https://en.wikipedia.org/wiki/Envelope_(music) */
	createADSRInput() {
		const adsrContainer = document.createElement("form");
		adsrContainer.addEventListener("submit", (e) => e.preventDefault());
		const adsrParams = Object.keys(this.adsr);
		adsrParams.forEach((param) => {
			const paramContainer = document.createElement("fieldset");
			const label = document.createElement("label");
			label.for = param;
			label.textContent = `${param.charAt(0).toUpperCase() + param.slice(1)}: `;

			const input = document.createElement("input");
			input.type = "number";
			input.name = param;
			input.min = 0;
			input.value = this.adsr[param];

			input.addEventListener("change", this.onADSR.bind(this));

			label.appendChild(input);
			paramContainer.appendChild(label);
			adsrContainer.appendChild(paramContainer);
		});

		return adsrContainer;
	}
}
