const DEFAULT_NOTE = {
	midi: null,
	startInBeats: null,
	durationInBeats: null,
	velocity: null,
};

export default class MidiNote extends HTMLElement {
	static get observedAttributes() {
		return ["midi", "start-in-beats", "duration-in-beats", "velocity"];
	}

	connectedCallback() {
		this.render();
	}

	attributeChangedCallback() {
		this.render();
	}

	handleInputChange(event) {
		const { name, value } = event.target;
		this.setAttribute(name, value);
		this.dispatchEvent(new CustomEvent("noteChange", { bubbles: true }));
	}

	handleClearNote() {
		Object.keys(DEFAULT_NOTE).forEach((key) =>
			this.setAttribute(key, DEFAULT_NOTE[key])
		);
		this.dispatchEvent(new CustomEvent("noteChange", { bubbles: true }));
	}

	render() {
		this.innerHTML = "";
		const form = document.createElement("form");

		attributes.forEach((attribute) => {
			const input = document.createElement("input");
			input.setAttribute("name", attribute.name);
			input.setAttribute("type", "number");
			input.value = this.getAttribute(attribute.name) || "";
			input.addEventListener("input", this.handleInputChange.bind(this));

			const label = document.createElement("label");
			label.textContent = attribute.label;
			label.appendChild(input);

			const fieldset = document.createElement("fieldset");
			fieldset.appendChild(label);
			form.appendChild(fieldset);
		});

		const deleteButton = document.createElement("button");
		deleteButton.innerText = "Clear";
		deleteButton.addEventListener("click", this.handleClearNote.bind(this));
		form.appendChild(deleteButton);

		this.appendChild(form);
	}
}
