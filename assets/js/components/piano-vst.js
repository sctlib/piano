export default class PianoVst extends HTMLElement {
	handlePianoNote({ detail: note, target, type: eventType }) {
		const { name, octave, midi } = note;
		const isOutputMidi = target.getAttribute("output-midi") === "true";
		if (true || !isOutputMidi) {
			if (eventType === "noteon") {
				this.$pianoAudio.playNote({
					name,
					octave,
					midi,
				});
			}
			if (eventType === "noteoff") {
				this.$pianoAudio.stopNote({
					name,
					octave,
					midi,
				});
			}
		}
	}
	handleSustainPedal({ type: eventType }) {
		if (eventType === "sustainon") {
			this.$pianoAudio.sustainOn();
		}
		if (eventType === "sustainoff") {
			this.$pianoAudio.sustainOff();
		}
	}
	connectedCallback() {
		this.$pianoAudio = this.createPianoAudio();
		this.$pianoMidi = this.createPianoMidi();
		this.$pianoLayout = this.createPianoLayout();
		this.render();
	}
	createPianoAudio() {
		return document.createElement("piano-audio");
	}
	createPianoMidi() {
		return document.createElement("piano-midi");
	}
	createPianoLayout() {
		const $pianoLayout = document.createElement("piano-layout");
		$pianoLayout.setAttribute("root-octave", 3);
		$pianoLayout.setAttribute("octaves", 4);
		$pianoLayout.setAttribute("modal-shift", 0);
		$pianoLayout.setAttribute("show-octaves", true);
		$pianoLayout.setAttribute("show-notes", true);
		$pianoLayout.setAttribute("output-midi", true);
		$pianoLayout.setAttribute("input-midi", true);
		$pianoLayout.setAttribute("input-midi-clock", true);
		$pianoLayout.setAttribute("input-mouse", true);
		$pianoLayout.setAttribute("input-keyboard", true);
		$pianoLayout.setAttribute("show-input-keyboard", true);
		$pianoLayout.addEventListener("noteon", this.handlePianoNote.bind(this));
		$pianoLayout.addEventListener("noteoff", this.handlePianoNote.bind(this));
		$pianoLayout.addEventListener(
			"sustainon",
			this.handleSustainPedal.bind(this)
		);
		$pianoLayout.addEventListener(
			"sustainoff",
			this.handleSustainPedal.bind(this)
		);
		return $pianoLayout;
	}
	render() {
		this.innerHTML = "";
		this.append(this.$pianoAudio);
		this.append(this.$pianoMidi);
		this.append(this.$pianoLayout);
	}
}
