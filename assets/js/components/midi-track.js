export default class MidiTrack extends HTMLElement {
	static get observedAttributes() {
		return ["grid-size"];
	}

	connectedCallback() {
		this.render();
	}

	attributeChangedCallback(attrName, oldVal, newVal) {
		if (attrName === "grid-size") {
			this.render();
		}
	}

	handleNoteChange() {
		this.dispatchEvent(new CustomEvent("trackUpdate", { bubbles: true }));
	}

	createNote() {
		const note = document.createElement("midi-note");
		note.addEventListener("noteChange", this.handleNoteChange.bind(this));
		return note;
	}

	handleAddNote() {
		const note = this.createNote();
		this.appendChild(note);
		this.handleNoteChange();
	}

	handleDeleteNote() {
		const lastNote = this.lastElementChild;
		if (lastNote) {
			lastNote.remove();
			this.handleNoteChange();
		}
	}

	render() {
		this.innerHTML = "";
		const addButton = document.createElement("button");
		addButton.innerText = "Add Note";
		addButton.addEventListener("click", this.handleAddNote.bind(this));

		const deleteButton = document.createElement("button");
		deleteButton.innerText = "Delete Note";
		deleteButton.addEventListener("click", this.handleDeleteNote.bind(this));

		this.appendChild(addButton);
		this.appendChild(deleteButton);
	}
}
