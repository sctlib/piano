import { defineComponents } from "../js-utils/index.js";

import PianoLayout from "./components/piano-layout.js";
import PianoOctave from "./components/piano-octave.js";
import PianoNote from "./components/piano-note.js";
import PianoAudio from "./components/piano-audio.js";
import PianoMidi from "./components/piano-midi.js";
import PianoVst from "./components/piano-vst.js";
import PianoApp from "./components/piano-app.js";
import MidiSequencer from "./components/midi-sequencer.js";

const componentDefinitions = {
	"piano-layout": PianoLayout,
	"piano-octave": PianoOctave,
	"piano-note": PianoNote,
	"piano-audio": PianoAudio,
	"piano-midi": PianoMidi,
	"piano-vst": PianoVst,
	"piano-app": PianoApp,
	"midi-sequencer": MidiSequencer,
};

defineComponents(componentDefinitions);

export {
	PianoLayout,
	PianoOctave,
	PianoNote,
	PianoAudio,
	PianoMidi,
	PianoVst,
	PianoApp,
	MidiSequencer,
};
