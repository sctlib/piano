import { CHROMATIC_SCALE_NOTES } from "./config.js";

/*
	 General Midi (GM MIDI)
	 https://en.wikipedia.org/wiki/General_MIDI
 */

/* 0x0F (binary: 00001111) and 0xF0 (binary: 11110000) */
const MIDI_COMMAND_MASK = 0xf0; // Mask to extract the MIDI command type
const MIDI_CHANNEL_MASK = 0xf; // Mask to extract the MIDI channel
const TOP_MIDI_NOTE = 127;

const MIDI_MESSAGES = {
	noteOn: 0x90,
	noteOff: 0x80,
	polyAftertouch: 0xa0,
	controlChange: 0xb0,
	programChange: 0xc0,
	channelAftertouch: 0xd0,
	pitchBend: 0xe0,
	systemExclusiveStart: 0xf0,
	timeCodeQuarterFrame: 0xf1,
	songPositionPointer: 0xf2,
	songSelect: 0xf3,
	tuneRequest: 0xf6,
	systemExclusiveEnd: 0xf7,
	timingClock: 0xf8,
	start: 0xfa,
	continue: 0xfb,
	stop: 0xfc,
	activeSensing: 0xfe,
	systemReset: 0xff,
};

const MIDI_CONTROL_CHANGES = {
	modulation: 1,
	volume: 7,
	channelPan: 10,
	expressionController: 11,
	ribbonController: 16,
	sustain: 64,
	sustainLevel: 79,
	decayGeneric: 80,
	lsb: 100,
	msb: 101,
	resetAllControllers: 121,
	allNotes: 123,
};

const MIDI_INSTRUMENTS_MAP = new Map([
	["acoustic-grand-piano", { category: "piano", name: "Acoustic Grand Piano" }],
	[
		"bright-acoustic-piano",
		{ category: "piano", name: "Bright Acoustic Piano" },
	],
	["electric-grand-piano", { category: "piano", name: "Electric Grand Piano" }],
	["honky-tonk-piano", { category: "piano", name: "Honky-tonk Piano" }],
	["electric-piano-1", { category: "piano", name: "Electric Piano 1" }],
	["electric-piano-2", { category: "piano", name: "Electric Piano 2" }],
	["harpsichord", { category: "piano", name: "Harpsichord" }],
	["clavinet", { category: "piano", name: "Clavinet" }],
	["celesta", { category: "chromatic-percussion", name: "Celesta" }],
	["glockenspiel", { category: "chromatic-percussion", name: "Glockenspiel" }],
	["music-box", { category: "chromatic-percussion", name: "Music Box" }],
	["vibraphone", { category: "chromatic-percussion", name: "Vibraphone" }],
	["marimba", { category: "chromatric-instrument", name: "Marimba" }],
	["xylophone", { category: "chromatric-instrument", name: "Xylophone" }],
	[
		"tubular-bells",
		{ category: "chromatric-instrument", name: "Tubular Bells" },
	],
	["dulcimer", { category: "chromatric-instrument", name: "Dulcimer" }],
	["drawbar-organ", { category: "organ", name: "Drawbar Organ" }],
	["percussive-organ", { category: "organ", name: "Percussive Organ" }],
	["rock-organ", { category: "organ", name: "Rock Organ" }],
	["church-organ", { category: "organ", name: "Church Organ" }],
	["reed-organ", { category: "organ", name: "Reed Organ" }],
	["accordion", { category: "organ", name: "Accordion" }],
	["harmonica", { category: "organ", name: "Harmonica" }],
	["tango-accordion", { category: "organ", name: "Tango Accordion" }],
	[
		"acoustic-guitar-nylon",
		{ category: "guitar", name: "Acoustic Guitar (nylon)" },
	],
	[
		"acoustic-guitar-steel",
		{ category: "guitar", name: "Acoustic Guitar (steel)" },
	],
	[
		"electric-guitar-jazz",
		{ category: "guitar", name: "Electric Guitar (jazz)" },
	],
	[
		"electric-guitar-clean",
		{ category: "guitar", name: "Electric Guitar (clean)" },
	],
	[
		"electric-guitar-muted",
		{ category: "guitar", name: "Electric Guitar (muted)" },
	],
	["overdriven-guitar", { category: "guitar", name: "Overdriven Guitar" }],
	["distortion-guitar", { category: "guitar", name: "Distortion Guitar" }],
	["guitar-harmonics", { category: "guitar", name: "Guitar harmonics" }],
	["acoustic-bass", { category: "bass", name: "Acoustic Bass" }],
	[
		"electric-bass-finger",
		{ category: "bass", name: "Electric Bass (finger)" },
	],
	["electric-bass-pick", { category: "bass", name: "Electric Bass (pick)" }],
	["fretless-bass", { category: "bass", name: "Fretless Bass" }],
	["slap-bass-1", { category: "bass", name: "Slap Bass 1" }],
	["slap-bass-2", { category: "bass", name: "Slap Bass 2" }],
	["synth-bass-1", { category: "bass", name: "Synth Bass 1" }],
	["synth-bass-2", { category: "bass", name: "Synth Bass 2" }],
	["violin", { category: "strings", name: "Violin" }],
	["viola", { category: "strings", name: "Viola" }],
	["cello", { category: "strings", name: "Cello" }],
	["contrabass", { category: "strings", name: "Contrabass" }],
	["tremolo-strings", { category: "strings", name: "Tremolo Strings" }],
	["pizzicato-strings", { category: "strings", name: "Pizzicato Strings" }],
	["orchestral-harp", { category: "strings", name: "Orchestral Harp" }],
	["timpani", { category: "strings", name: "Timpani" }],
	["string-ensemble-1", { category: "ensemble", name: "String Ensemble 1" }],
	["string-ensemble-2", { category: "ensemble", name: "String Ensemble 2" }],
	["synth-strings-1", { category: "ensemble", name: "Synth Strings 1" }],
	["synth-strings-2", { category: "ensemble", name: "Synth Strings 2" }],
	["choir-aahs", { category: "ensemble", name: "Choir Aahs" }],
	["voice-oohs", { category: "ensemble", name: "Voice Oohs" }],
	["synth-voice", { category: "ensemble", name: "Synth Voice" }],
	["orchestra-hit", { category: "ensemble", name: "Orchestra Hit" }],
	["trumpet", { category: "brass", name: "Trumpet" }],
	["trombone", { category: "brass", name: "Trombone" }],
	["tuba", { category: "brass", name: "Tuba" }],
	["muted-trumpet", { category: "brass", name: "Muted Trumpet" }],
	["french-horn", { category: "brass", name: "French Horn" }],
	["brass-section", { category: "brass", name: "Brass Section" }],
	["synth-brass-1", { category: "brass", name: "Synth Brass 1" }],
	["synth-brass-2", { category: "brass", name: "Synth Brass 2" }],
	["soprano-sax", { category: "reed", name: "Soprano Sax" }],
	["alto-sax", { category: "reed", name: "Alto Sax" }],
	["tenor-sax", { category: "reed", name: "Tenor Sax" }],
	["baritone-sax", { category: "reed", name: "Baritone Sax" }],
	["oboe", { category: "reed", name: "Oboe" }],
	["english-horn", { category: "reed", name: "English Horn" }],
	["bassoon", { category: "reed", name: "Bassoon" }],
	["clarinet", { category: "reed", name: "Clarinet" }],
	["piccolo", { category: "pipe", name: "Piccolo" }],
	["flute", { category: "pipe", name: "Flute" }],
	["recorder", { category: "pipe", name: "Recorder" }],
	["pan-flute", { category: "pipe", name: "Pan Flute" }],
	["blown-bottle", { category: "pipe", name: "Blown Bottle" }],
	["shakuhachi", { category: "pipe", name: "Shakuhachi" }],
	["whistle", { category: "pipe", name: "Whistle" }],
	["ocarina", { category: "pipe", name: "Ocarina" }],
	["lead-1-square", { category: "synth-lead", name: "Lead 1 (square)" }],
	["lead-2-sawtooth", { category: "synth-lead", name: "Lead 2 (sawtooth)" }],
	["lead-3-calliope", { category: "synth-lead", name: "Lead 3 (calliope)" }],
	["lead-4-chiff", { category: "synth-lead", name: "Lead 4 (chiff)" }],
	["lead-5-charang", { category: "synth-lead", name: "Lead 5 (charang)" }],
	["lead-6-voice", { category: "synth-lead", name: "Lead 6 (voice)" }],
	["lead-7-fifths", { category: "synth-lead", name: "Lead 7 (fifths)" }],
	[
		"lead-8-bass-lead",
		{ category: "synth-lead", name: "Lead 8 (bass + lead)" },
	],
	["pad-1-new-age", { category: "synth-pad", name: "Pad 1 (new age)" }],
	["pad-2-warm", { category: "synth-pad", name: "Pad 2 (warm)" }],
	["pad-3-polysynth", { category: "synth-pad", name: "Pad 3 (polysynth)" }],
	["pad-4-choir", { category: "synth-pad", name: "Pad 4 (choir)" }],
	["pad-5-bowed", { category: "synth-pad", name: "Pad 5 (bowed)" }],
	["pad-6-metallic", { category: "synth-pad", name: "Pad 6 (metallic)" }],
	["pad-7-halo", { category: "synth-pad", name: "Pad 7 (halo)" }],
	["pad-8-sweep", { category: "synth-pad", name: "Pad 8 (sweep)" }],
	["fx-1-rain", { category: "synth-effects", name: "FX 1 (rain)" }],
	["fx-2-soundtrack", { category: "synth-effects", name: "FX 2 (soundtrack)" }],
	["fx-3-crystal", { category: "synth-effects", name: "FX 3 (crystal)" }],
	["fx-4-atmosphere", { category: "synth-effects", name: "FX 4 (atmosphere)" }],
	["fx-5-brightness", { category: "synth-effects", name: "FX 5 (brightness)" }],
	["fx-6-goblins", { category: "synth-effects", name: "FX 6 (goblins)" }],
	["fx-7-echoes", { category: "synth-effects", name: "FX 7 (echoes)" }],
	["fx-8-sci-fi", { category: "synth-effects", name: "FX 8 (sci-fi)" }],
	["sitar", { category: "ethnic", name: "Sitar" }],
	["banjo", { category: "ethnic", name: "Banjo" }],
	["shamisen", { category: "ethnic", name: "Shamisen" }],
	["koto", { category: "ethnic", name: "Koto" }],
	["kalimba", { category: "ethnic", name: "Kalimba" }],
	["bag-pipe", { category: "ethnic", name: "Bag pipe" }],
	["fiddle", { category: "ethnic", name: "Fiddle" }],
	["shanai", { category: "ethnic", name: "Shanai" }],
	["tinkle-bell", { category: "percussive", name: "Tinkle Bell" }],
	["agogô", { category: "percussive", name: "Agogô" }],
	["steel-drums", { category: "percussive", name: "Steel Drums" }],
	["woodblock", { category: "percussive", name: "Woodblock" }],
	["taiko-drum", { category: "percussive", name: "Taiko Drum" }],
	["melodic-tom", { category: "percussive", name: "Melodic Tom" }],
	["synth-drum", { category: "percussive", name: "Synth Drum" }],
	["reverse-cymbal", { category: "percussive", name: "Reverse Cymbal" }],
	[
		"guitar-fret-noise",
		{ category: "sound-effects", name: "Guitar Fret Noise" },
	],
	["breath-noise", { category: "sound-effects", name: "Breath Noise" }],
	["seashore", { category: "sound-effects", name: "Seashore" }],
	["bird-tweet", { category: "sound-effects", name: "Bird Tweet" }],
	["telephone-ring", { category: "sound-effects", name: "Telephone Ring" }],
	["helicopter", { category: "sound-effects", name: "Helicopter" }],
	["applause", { category: "sound-effects", name: "Applause" }],
	["gunshot", { category: "sound-effects", name: "Gunshot" }],
]);

const MIDI_PERCUSSIONS_MAP = new Map([
	[
		"acoustic-bass-drum",
		{ category: "percussions", name: "Acoustic Bass Drum" },
	],
	["bass-drum-1", { category: "percussions", name: "Bass Drum 1" }],
	["side-stick", { category: "percussions", name: "Side Stick" }],
	["acoustic-snare", { category: "percussions", name: "Acoustic Snare" }],
	["hand-clap", { category: "percussions", name: "Hand Clap" }],
	["electric-snare", { category: "percussions", name: "Electric Snare" }],
	["low-floor-tom", { category: "percussions", name: "Low Floor Tom" }],
	["closed-hi-hat", { category: "percussions", name: "Closed Hi Hat" }],
	["high-floor-tom", { category: "percussions", name: "High Floor Tom" }],
	["pedal-hi-hat", { category: "percussions", name: "Pedal Hi-Hat" }],
	["low-tom", { category: "percussions", name: "Low Tom" }],
	["open-hi-hat", { category: "percussions", name: "Open Hi-Hat" }],
	["low-mid-tom", { category: "percussions", name: "Low-Mid Tom" }],
	["hi-mid-tom", { category: "percussions", name: "Hi-Mid Tom" }],
	["crash-cymbal-1", { category: "percussions", name: "Crash Cymbal 1" }],
	["high-tom", { category: "percussions", name: "High Tom" }],
	["ride-cymbal-1", { category: "percussions", name: "Ride Cymbal 1" }],
	["chinese-cymbal", { category: "percussions", name: "Chinese Cymbal" }],
	["ride-bell", { category: "percussions", name: "Ride Bell" }],
	["tambourine", { category: "percussions", name: "Tambourine" }],
	["splash-cymbal", { category: "percussions", name: "Splash Cymbal" }],
	["cowbell", { category: "percussions", name: "Cowbell" }],
	["crash-cymbal-2", { category: "percussions", name: "Crash Cymbal 2" }],
	["vibraslap", { category: "percussions", name: "Vibraslap" }],
	["ride-cymbal-2", { category: "percussions", name: "Ride Cymbal 2" }],
	["hi-bongo", { category: "percussions", name: "Hi Bongo" }],
	["low-bongo", { category: "percussions", name: "Low Bongo" }],
	["mute-hi-conga", { category: "percussions", name: "Mute Hi Conga" }],
	["open-hi-conga", { category: "percussions", name: "Open Hi Conga" }],
	["low-conga", { category: "percussions", name: "Low Conga" }],
	["high-timbale", { category: "percussions", name: "High Timbale" }],
	["low-timbale", { category: "percussions", name: "Low Timbale" }],
	["high-agogo", { category: "percussions", name: "High Agogo" }],
	["low-agogo", { category: "percussions", name: "Low Agogo" }],
	["cabasa", { category: "percussions", name: "Cabasa" }],
	["maracas", { category: "percussions", name: "Maracas" }],
	["short-whistle", { category: "percussions", name: "Short Whistle" }],
	["long-whistle", { category: "percussions", name: "Long Whistle" }],
	["short-guiro", { category: "percussions", name: "Short Guiro" }],
	["long-guiro", { category: "percussions", name: "Long Guiro" }],
	["claves", { category: "percussions", name: "Claves" }],
	["hi-wood-block", { category: "percussions", name: "Hi Wood Block" }],
	["low-wood-block", { category: "percussions", name: "Low Wood Block" }],
	["mute-cuica", { category: "percussions", name: "Mute Cuica" }],
	["open-cuica", { category: "percussions", name: "Open Cuica" }],
	["mute-triangle", { category: "percussions", name: "Mute Triangle" }],
]);

function getMidiProgram(program, channel) {
	if (channel === 9) {
		const percussion = getPercussionFromProgram(program);
		if (percussion) {
			return percussion.instrument;
		}
	} else {
		const instrument = getInstrumentFromProgram(program);
		if (instrument) {
			return instrument;
		}
	}
	return null;
}
function getInstrumentFromProgram(program) {
	return MIDI_INSTRUMENTS_MAP.get(program);
}
function getPercussionFromProgram(program) {
	return MIDI_PERCUSSIONS_MAP.get(program);
}

/* from a `midimessage` event data, get its meaning */
function getMidimessageInfo(midimessageData) {
	const MIDI_COMMAND_MASK = 0xf0; // Mask to extract the MIDI command type
	const MIDI_CHANNEL_MASK = 0xf; // Mask to extract the MIDI channel
	const midiInfo = {
		// Extract the command by shifting 4 bits to the right
		// command is in the higher 4 bits, channel in the lower 4 bits.
		command: midimessageData[0] >> 4,
		// Extract the channel by applying the channel mask
		channel: midimessageData[0] & MIDI_CHANNEL_MASK,
		// Extract the command type by applying the command mask
		type: midimessageData[0] & MIDI_COMMAND_MASK,
		// First data byte, its meaning depends on the message type
		dataByte1: midimessageData[1],
		// Second data byte, is always the velocity, and meaning varies
		velocity: midimessageData[2],
	};
	return midiInfo;
}

const generateChromaticMidiNotes = (scaleNotes = CHROMATIC_SCALE_NOTES) => {
	const octaves = 11;
	const rootMidiCode = 0; // C-1
	const offset = scaleNotes.length;
	const chromaticMidiNotes = {};
	scaleNotes.forEach((note, noteChromaticIndex) => {
		const midiNotes = generateMidiForNote({
			length: octaves,
			rootMidiCode,
			scaleOffset: offset,
			noteOffset: noteChromaticIndex,
		});
		chromaticMidiNotes[note.name] = midiNotes;
	});
	return chromaticMidiNotes;
};

const generateMidiForNote = ({
	length,
	rootMidiCode,
	scaleOffset,
	noteOffset,
}) => {
	const allNotes = Array(length)
		.fill(0)
		.map((_, index) => {
			return rootMidiCode + scaleOffset * index + noteOffset;
		});
	return allNotes.filter((note) => note <= TOP_MIDI_NOTE);
};

const generateMidiNotesMap = (allMidiByNotes) => {
	const midiNotesMap = {};
	Object.keys(allMidiByNotes).forEach((noteName, nodeChromaticIndex) => {
		const noteMidiCodes = allMidiByNotes[noteName];
		noteMidiCodes.forEach((noteMidiCode, noteOctaveIndex) => {
			midiNotesMap[noteMidiCode] = {
				octave: noteOctaveIndex,
				midi: noteMidiCode,
				name: noteName,
			};
		});
	});
	return midiNotesMap;
};

const MIDI_NOTES = generateChromaticMidiNotes(CHROMATIC_SCALE_NOTES);
const MIDI_NOTES_MAP = generateMidiNotesMap(MIDI_NOTES);

export {
	MIDI_INSTRUMENTS_MAP,
	MIDI_PERCUSSIONS_MAP,
	getMidiProgram,
	getInstrumentFromProgram,
	getPercussionFromProgram,
	getMidimessageInfo,
	MIDI_MESSAGES,
	MIDI_CONTROL_CHANGES,
	MIDI_NOTES,
	MIDI_NOTES_MAP,
	generateMidiNotesMap,
	generateChromaticMidiNotes as generateMidiNotes,
};
