/* Utilities to use for static websites;
	 - where the content is static html (maybe generated from .md or .org etc.),
	 - there is no js-client-site-router (maybe there could be)
	 - some UI components can be insert through web-components in different pages
 */

/* This constant is relative to the project's folder root:
	 /assets/js-utils/index.js
	 |---1--|----2---|---3---|
 */
const SITE_UTILS_NESTING = 3;
const SITE_UTILS_PATHS = import.meta.url.split("/");
const SITE_BASE = SITE_UTILS_PATHS.slice(
	0,
	SITE_UTILS_PATHS.length - SITE_UTILS_NESTING
).join("/");

/* examples of the models */
const COMPONENT_DEFINITIONS_EX = {
	"html-element": HTMLElement,
};

/* A function to define web components,
	 from an entire map of web components html tag name and their class,
	 and a list of selected components
	 if not already defined.*/
const defineComponents = (
	allComponentDefinitions = COMPONENT_DEFINITIONS_EX,
	selectedComponents = Object.keys(allComponentDefinitions)
) => {
	/* fill in all components by default if none selected */
	if (selectedComponents.length === 0) {
		selectedComponents = [...Object.keys(allComponentDefinitions)];
	}
	/* go through the list of all components, and define matching selected */
	for (const [componentName, componentClass] of Object.entries(
		allComponentDefinitions
	)) {
		if (
			selectedComponents.includes(componentName) &&
			!window.customElements.get(componentName)
		) {
			window.customElements.define(componentName, componentClass);
		}
	}
};

/* insert html elements in the dom,
	 from their tag/element name, or config */
const insertComponents = ({
	/* list of elements to insert in the DOM (head/body), COMPONENT_TAGS_EX */
	headElements = [],
	bodyStartElements = [],
	bodyEndElements = [],
	headElement = "head",
	bodyElement = "body",
}) => {
	const $head = document.querySelector(headElement);
	const $body = document.querySelector(bodyElement);

	/* instanciate each element in their respective list/group */
	const domConfig = Object.entries({
		headElements,
		bodyStartElements,
		bodyEndElements,
	}).reduce((acc, [domGroup, domElements]) => {
		acc[domGroup] = domElements.map((domElement) => {
			return createElement(domElement);
		});
		return acc;
	}, {});

	/* position the elements in the dom where they should */
	$head.append(...domConfig.headElements);
	$body.append(...domConfig.bodyEndElements);
	$body.prepend(...domConfig.bodyStartElements);
};

/* create a DOM element, from its tag/element name or config */
const createElement = (tagOrConfig) => {
	if (typeof tagOrConfig === "string") {
		return document.createElement(tagOrConfig);
	} else if (typeof tagOrConfig === "object") {
		return createElementWithAttributes(tagOrConfig);
	}
};

/* create a dom element from a config made of,
	 its element/tag name, HTML attributes, and innerText */
const createElementWithAttributes = (elementConfig) => {
	const { element } = elementConfig;
	const attributes = { ...elementConfig };
	delete attributes.element;
	const $element = document.createElement(element);
	Object.entries(attributes).forEach(([attrName, attrVal]) => {
		if (attrName === "innerHTML") {
			/* $element.innerHTML = innerHTML; */
		} else if (attrName === "innerText") {
			$element.innerText = attrVal;
		} else {
			if (typeof attrVal === "object") {
				$element.setAttribute(attrName, JSON.stringify(attrVal));
			} else {
				$element.setAttribute(attrName, attrVal);
			}
		}
	});
	return $element;
};

/*
	 gets the HTML script tag, which is executing the code calling this function
	 https://stackoverflow.com/questions/403967/how-may-i-reference-the-script-tag-that-loaded-the-currently-executing-script */
const getCurrentScript = () => {
	const $scripts = globalThis.document.getElementsByTagName("script");
	const $me = $scripts[$scripts.length - 1];
	return $me;
};

/* utilities to remove element(s) from the DOM, maybe,
	 for example, a script that inserts DOM content, and can later be removed
	 (so it cleans the DOM)
 */
const deleteElement = ($domElement) => {
	return $domElement.remove();
};
const deleteElements = ($domElements) => {
	return $domElements.map(deleteElement);
};

/* insert a DOM element before/after an oter one (from accessing its parent)
	 https://developer.mozilla.org/en-US/docs/Web/API/Node/insertBefore */
const insertBefore = (newNode, refNode) => {
	refNode.parentElement.insertBefore(newNode, refNode);
};
/* after does not exist but can be emulated with insertBefore & nextSibling */
const insertAfter = (newNode, refNode) => {
	refNode.parentElement.insertBefore(newNode, refNode.nextSibling);
};

export {
	SITE_BASE,
	defineComponents,
	insertComponents,
	createElementWithAttributes,
	createElement,
	getCurrentScript,
	deleteElement,
	deleteElements,
	insertBefore,
	insertAfter,
};
