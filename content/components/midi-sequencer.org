* =<midi-sequencer>=
The piano-sequencer generaters a midi sequencer

** default
When inserted in a page's HTML, by default, it shows one octave of the piano, from =C= to =C=.
#+begin_src html
<midi-sequencer></midi-sequencer>
#+end_src
#+begin_export html
<midi-sequencer></midi-sequencer>
#+end_export
