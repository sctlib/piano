#+TITLE: tools
#+AUTHOR: sctlib

Because this site is built using the open web APIs built-in most
web-browsers, as well as the [[https://en.wikipedia.org/wiki/MIDI][MIDI standard]], it is possible to use each
piano element as midi input and/or output, with other pages, or midi
devices detected by the browser.

** MIDI
The midi settings are on the right edge of the piano layout:
- midi-input: the piano-layout will listen for incoming MIDI messages
  from the specified source
- midi-output: the piano-kayout will send MIDI messages to the
  specified destination

Don't hesitate to refresh each of the browser pages when midi
input/outputs are not working as expected (and open and let it be known).

** To record notes as MIDI
- https://pianito.netlify.app is a web-app that can be used to record
  the notes played on the piano, in MIDI format (and share it via a
  URL).

** To find out the keys from the notes

- https://partoche.netlify.app is a web-app, that works with midi inputs.

If you open the link above in an other page, you can you this current
page's piano as midi piano, to the =partoche= (and =pianito= apps).

For this, select these app's midi as =output= to this page's main
 =piano-layout= (at the bottom of the page).

** piano web-components
The pages of this document are built using the piano-layout web
component and the sounds from the web audio api(s). They are
available as [[file:components/index.org][web-components]] to re-use on other web-sites.
