#+TITLE: info/about
#+AUTHOR: sctlib

About this project/site.

#+begin_quote
This is an ongoing experimental project. Requests, contributions and
comments are welcome. There is inexact information that should be
rectified.
#+end_quote

- [[https://matrix.to/#/#piano:sctlib.org][matrix space]] chat
- [[mailto:incoming+sctlib-piano-33317124-issue-@incoming.gitlab.com][email]] contact
- [[https://gitlab.com/sctlib/piano/][code on gitlab]] ([[https://www.gnu.org/licenses/gpl-3.0.en.html][GPLv3]])
- Content & visual assets: Creative Commons
  [[https://creativecommons.org/licenses/by-nc-sa/4.0/][Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA
  4.0)]].
